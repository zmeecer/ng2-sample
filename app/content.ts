import {Component} from 'angular2/core';

@Component({
    selector: 'content',
    template: `
      <div>
          <button (click)="addContent($event)">create content</button>
          <div>{{ state.content || "No content" }}</div>
      </div>
    `
})
export class Content {
    state = {
      content: "",
    };

    addContent(e) {
      this.state.content = `${this.state.content} lorem ipsum`
    }
}
