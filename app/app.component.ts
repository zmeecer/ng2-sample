import {Component} from 'angular2/core';
import {Content} from './content';
import {HeroComponent} from './hero';

@Component({
    selector: 'my-app',
    styles: [`
      .yellow {
        background-color: yellow;
      }
    `],
    directives: [
      Content,
      HeroComponent
    ],
    template:
      `<div>
          <h1>{{ state.message }}</h1>
          <content></content>
          <button (click)="changeColor(box)">change color</button>
          <div class="box" #box>color</div>
          <hero></hero>
      </div>`
})
export class AppComponent {
    state = {
      message: 'My First Angular 2 App',
    };

    changeColor(box) {
      box.classList.add('yellow')
    }
}
